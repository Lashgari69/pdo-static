<?php

class DataBase
{
    private static $con;

    public static function connect()
    {
        try
        {
            self::$con = new PDO("mysql:host=localhost;dbname=pdo","root","123456");
            $connection = self::$con;
            $connection->exec("set names utf8");
            return $connection;
        }
        catch (Exception $e){
               echo $e->getMessage();
        }
    }

}

DataBase::connect();